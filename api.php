<?php
require_once 'lib/class.db.php';
require_once 'lib/class.api.php';

class MyAPI extends API
{
    protected function example()
    {
        if ($this->method == 'GET') {
            return "Your name is julius";
        } else {
            return "Only accepts GET requests";
        }
    }

    function freespace($TreatmentID)
    {

        //$TreatmentID = $_GET['treatmentid'];

        $StartTime = date("Y-m-d H:i:s");
        $StartTime = strtotime($StartTime);
        $EndTime = strtotime("+7 day", $StartTime);
        $Db = new Database;
        // Get treatmentdata
        $FreeSpace = array();

        $treatmentdata = $Db->select("SELECT *
            FROM HL_Treatments WHERE  TreatmentID = '" . $TreatmentID . "' 
        	");
        foreach ($treatmentdata AS $treatmentkey => $treatmentvalue) {
            $duration = 0;
            $ProductArray = explode(",", $treatmentvalue['ProductArray']);
            // echo "test : " . $ProductArray;
            foreach ($ProductArray AS $ProductKeys) {

                $tproductdata = $Db->select("SELECT *
                        FROM HL_Products WHERE  ProductID = '" . $ProductKeys . "' 
                        ");
                foreach ($tproductdata AS $ProductDataKeys => $ProductDataValue) {
                    // echo "Productdata : " . $ProductDataValue['Duration'];
                    $duration = $duration + $ProductDataValue['Duration'];
                    $newReservationProductArray[] = array('Duration' => $ProductDataValue['Duration'], 'ProductCode' => $ProductDataValue['ProductCode']);
                }
                //$newReservationProductArray[] = array('Duration' => $duration, )

            }
            //$newReservationProductArray[] = array('Duration' => $duration );


        }
        $min = (int)$duration;
        $hours = (int)($min / 60);
        $minutes = (($min / 60) - $hours) * 60;
        $durationTime = $hours . ":" . $minutes;

        //echo "Duration :  ". $duration ."<br/><br/>";
        //print_r($newReservationProductArray);
        //echo "<br/>Endtime (next seven days) ".strftime(date("Y-m-d H:i:s",$EndTime)). "<br/><br/>";

        $workingblockdata = $Db->select("SELECT DISTINCT a.*, concat(b.FirstName, \" \" ,b.Prefix, \" \" , b.LastName) AS EmployeeName
            FROM HL_WorkingBlocks a 
             LEFT JOIN HL_Employees b ON a.EmployeeID = b.EmployeeID
				WHERE
				    `StartTime` < `EndTime` AND 
					`StartTime` > '" . strftime(date("Y-m-d H:i:s", $StartTime)) . "' AND `EndTime` < '" . strftime(date("Y-m-d H:i:s", $EndTime)) . "'
				 ORDER BY StartTime");
        /*echo "<pre>";
         print_r($workingblockdata);
        echo "</pre>";*/


        foreach ($workingblockdata AS $workingblockkey => $workingblockvalue) {

            $begin = new DateTime($workingblockvalue['StartTime']);
            $end = new DateTime($workingblockvalue['EndTime']);
            // $end = $end->modify( '+1 day' );

            $interval = new DateInterval('PT15M');
            $daterange = new DatePeriod($begin, $interval, $end);
            $EmployeeCalendarTimeItem = array();

            foreach ($daterange as $date) {
                /* $EmployeeCalendarTimeItem = array();
                 //echo $date->format("d M H:i") . " -  ".$workingblockvalue['EmployeeName']. "<br>";
                 $EmployeeCalendarTimeItem[]['EmployeeID'] = $workingblockvalue['EmployeeID'];
                 $EmployeeCalendarTimeItem[]['StartTime'] = $date->format("H:i");
                 $EmployeeCalendarTimeItem[]['Date'] = $date->format("H:i");*/


            }


            $durationInTime = strtotime($durationTime);
            $maxendtime = strtotime($workingblockvalue['EndTime']);


            // print_r($range);
            foreach ($daterange as $date) {
                //echo "<h1>".$workingblockvalue['EmployeeName']."</h1>";
                //echo "<pre>";
                //print_r($date);
                //echo "</pre>";

                $loopdate = $date->format("Y-m-d");
                $looptime = $date->format("H:i");
                // $loopdate = $date->format("d M H:i");
                //echo $loopdate;
                $nexttime = strtotime("+15 minutes", strtotime($looptime));
                $newnexttime = date("H:i:s", $nexttime);


                $ExistingReservation = $Db->select("SELECT *
                FROM HL_ReservationDetails 
                    WHERE ReservationDate = '" . $loopdate . "' AND
                        `NewStartTime`  BETWEEN  '" . $looptime . "' AND '" . $newnextime . "'
                        AND `NewEndTime` <= '" . $newnextime . "' 
                      
                        
                        
                            ORDER BY ReservationDate DESC, NewStartTime DESC
                    ");


                if (empty($ExistingReservation)) {
                    // echo "Free time for selected Treatment".$workingblockvalue['EmployeeName']."<br/>" ;
                    $FreeSpace[] = array('ReservationDateTime' => $loopdate . " " . $looptime, 'ReservationDate' => $loopdate, 'StartTime' => $looptime, 'EmployeeName' => str_replace("   ", " ", $workingblockvalue['EmployeeName']), 'EmployeeID' => $workingblockvalue['EmployeeID']);
                }
                foreach ($FreeSpace AS $wbstartkey => $wbstartvalue) {
                    /* echo $wbstartkey." - ".$wbstartvalue."<br/>";
                     echo print_r($wbstartvalue);
                     echo "<br/>";*/

                }
            }


            // SELECT TIMESTAMPDIFF(SECOND, '2012-06-06 13:13:55', '2012-06-06 15:20:18')


        }
        usort($FreeSpace, 'date_compare');
        /* echo "<pre>";
         print_r( $FreeSpace);
         echo "</pre>";*/

        return $FreeSpace;
    }

    function sortFunction($a, $b)
    {
        return strtotime($a["time"]) - strtotime($b["time"]);
    }

    protected function treatments()
    {
        $Db = new Database;
        $data = $Db->select("SELECT * FROM `HL_Treatments`");
        //$treatments = array();
        foreach ($data as $treatmentkey => $treatmentvalue) {
            //$FullProductArray = array();

            //echo "<br/>". $treatmentkey ." --> " . $treatmentvalue['TreatmentName'] . $treatmentvalue['ProductArray'];

            if (strpos($treatmentvalue['ProductArray'], ',') !== false) {
                $ProductArray = explode(", ", $treatmentvalue['ProductArray']);
            } else {
                $ProductArray = array(0 => $treatmentvalue['ProductArray']);
            }


            foreach ($ProductArray as $productkey => $productvalue) {
                $ProductData = $Db->select("SELECT * FROM `HL_Products` WHERE  `ProductID` = $productvalue");
                // print_r($ProductData);
                foreach ($ProductData AS $key => $value) {
                    $FullProductArray[] = $value;
                }

            }

            $data[$treatmentkey]['Products'] = $FullProductArray;
            //$treatments['treatments'][] = $data[$treatmentkey];
            $FullProductArray = null;


        }


        // echo "Empty array output as object: ", json_encode($rows, JSON_FORCE_OBJECT), "\n
        return $data;
    }

    protected function treatment($TreatmentID)
    {
        $Db = new Database;
        $data = $Db->select("SELECT * FROM `HL_Treatments` WHERE TreatmentID  = '$TreatmentID' )");
        //$treatments = array();
        foreach ($data as $productkey => $productvalue) {
            $ProductArray = explode(",", $productvalue['ProductArray']);
            $treatmentdetails = array('TreatmentName' => $productvalue['TreatmentName'],
                'TreatmentID' => $productvalue['TreatmentID'],
                'Costs' => $productvalue['Costs'],
                'Class' => $productvalue['Class'],
                'Products' => $ProductArray
            );
        }
        // echo "Empty array output as object: ", json_encode($rows, JSON_FORCE_OBJECT), "\n
        return $treatmentdetails;
    }
    protected function client($clientid)
    {

        $Db = new Database;
        $sql = "SELECT * FROM `HL_Customers` WHERE  CustomerID = '".$clientid[0]."' LIMIT 0,1";
       // echo $sql;
        $data = $Db->select($sql);
        //$allclients = array();
        foreach ($data as $clientkey => $clientvalue) {
            $ReservationHistory= array();
            $Db = new Database;
            $reservationsql = "SELECT a.*, b.* FROM `HL_Reservations` a LEFT JOIN `HL_Treatments` b ON a.TreatmentID = b.TreatmentID WHERE a.CustomerID = '".$clientid[0]."' ORDER BY ReservationDate DESC";
            $reservationdata = $Db->select($reservationsql);
            foreach ($reservationdata as $reservationkey => $reservationvalue) {
                $ReservationHistory[] = array(
                    'ReservationDate' => $reservationvalue['ReservationDate'],
                    'TreatmentName' => $reservationvalue['TreatmentName']

                );

            }


            $allclients = array('ClientID' => $clientvalue['CustomerID'],
                'Email' => $clientvalue['Email'],
                'PhoneNumber' => $clientvalue['PhoneNumber'],
                'FirstName' => $clientvalue['FirstName'],
                'Prefix' => $clientvalue['Prefix'],
                'LastName' => $clientvalue['LastName'],
                'Memo' => $clientvalue['MemoField'],
                'ReservationHistory' => $ReservationHistory

            );


        }
        return $allclients;
    }
    protected function clients()
    {
        $Db = new Database;

        $data = $Db->select("SELECT MemoField, Email, PhoneNumber, FirstName, LastName, Prefix, CustomerID  FROM `HL_Customers` WHERE EXISTS (SELECT * FROM HL_Reservations WHERE ReservationDate >= '2017-01-01' AND HL_Customers.CustomerID = HL_Reservations.CustomerID) ORDER BY LastName ASC");
        //$treatments = array();
        foreach ($data as $clientkey => $clientvalue) {
            //$FullProductArray = array();
            //$Db = new Database;




            $allclients[] = array('ClientID' => $clientvalue['CustomerID'],
                'Email' => $clientvalue['Email'],
                'PhoneNumber' => $clientvalue['PhoneNumber'],
                 'FirstName' => $clientvalue['FirstName'],
                'Prefix' => $clientvalue['Prefix'],
                'LastName' => $clientvalue['LastName'],
                'Memo' => $clientvalue['MemoField'],
                //'ReservationArray' => $reservationdataArray

            );


        }
        return $allclients;
    }

    protected function employees()
    {
        $Db = new Database;
        $data = $Db->select("SELECT * ,  concat(FirstName, \" \" ,Prefix, \" \" , LastName) AS EmployeeName FROM `HL_Employees` ORDER BY LastName ASC");
        //$treatments = array();
        foreach ($data as $employeekey => $employeevalue) {
            //$FullProductArray = array();

            //echo "<br/>". $productkey ." --> " . $productvalue['productName'] . $productvalue['ProductArray'];


            $allemployees[] = array('EmployeeID' => $employeevalue['EmployeeID'], 'colorCode' => $employeevalue['colorCode'], 'EmployeeName' => $employeevalue['EmployeeName'], 'Email' => $employeevalue['Email'], 'Phone' => $employeevalue['Phone'], 'Internet' => $employeevalue['Internet']);


        }
        return $allemployees;
    }

    protected function employee($EmployeeID)
    {
        $Db = new Database;
        $data = $Db->select("SELECT * ,  concat(FirstName, \" \" ,Prefix, \" \" , LastName) AS EmployeeName FROM `HL_Employees` WHERE `EmployeeID` = '" . $EmployeeID . "' ORDER BY LastName");
        //$treatments = array();

        foreach ($data as $productkey => $productvalue) {
            $allproducts[] = array('ProductName' => $productvalue['ProductName'], 'ProductID' => $productvalue['ProductID'], 'Costs' => $productvalue['Costs'], 'Duration' => $productvalue['Duration']);
        }
        // echo "Empty array output as object: ", json_encode($rows, JSON_FORCE_OBJECT), "\n
        return $allproducts;
    }

    protected function workingblocks($EmployeeID)
    {  // echo "SELECT * FROM `HL_WorkingBlocks` WHERE EmployeeID = '".$EmployeeID[0][EmployeeID]."'";
        $Db = new Database;
        $data = $Db->select("SELECT * FROM `HL_WorkingBlocks` WHERE EmployeeID = '".$EmployeeID[0]."'");
        //$allworkingblocks = array();
        foreach ($data as $productkey => $productvalue) {

            $allworkingblocks[] = array('allDay' => '', 'id' => $productvalue['WorkingBlockID'], 'title' => $productvalue['EmployeeID'], 'start' => str_replace(" ", "T",$productvalue['StartTime']), 'end' => str_replace(" ", "T",$productvalue['EndTime']));
        }
        return $allworkingblocks;
    }

    protected function products()
    {
        $Db = new Database;
        $data = $Db->select("SELECT * FROM `HL_Products`");
        //$treatments = array();
        foreach ($data as $productkey => $productvalue) {

            $allproducts[] = array('ProductName' => $productvalue['ProductName'], 'ProductCode' => $productvalue['ProductCode'], 'ProductID' => $productvalue['ProductID'], 'Costs' => $productvalue['Costs'], 'Duration' => $productvalue['Duration']);
        }
        return $allproducts;
    }

    protected function product($TreatmentID)
    {
        $Db = new Database;
        $data = $Db->select("SELECT * FROM `HL_Products` WHERE ProductID IN (SELECT ProductArray FROM HL_Treatments WHERE TreatmentID = '".$TreatmentID."' )");
        //$treatments = array();

        foreach ($data as $productkey => $productvalue) {
            $allproducts[] = array('ProductName' => $productvalue['ProductName'],'ProductCode' => $productvalue['ProductCode'], 'ProductID' => $productvalue['ProductID'], 'Costs' => $productvalue['Costs'], 'Duration' => $productvalue['Duration']);
        }
        // echo "Empty array output as object: ", json_encode($rows, JSON_FORCE_OBJECT), "\n
        return $allproducts;
    }


    protected function availability()
    {

        $Db = new Database;
        $Employees = $Db->select("SELECT CONCAT(FirstName,\" \", Prefix, \" \", LastName) AS EmployeeName, Picture, ColorCode, EmployeeID 
            FROM HL_Employees a
               WHERE Online = '1'");


        foreach ($Employees AS $EmployeeKey => $EmployeeValue) {
            $Workingblocks = $Db->select(" SELECT * FROM HL_WorkingBlocks 
                        WHERE EmployeeID = '" . $EmployeeValue['EmployeeID'] . "' AND `StartTime` < `EndTime`  AND `StartTime` != `EndTime` AND `StartTime` BETWEEN NOW() AND NOW() + INTERVAL 30 DAY  
                        ORDER BY StartTime");
            foreach ($Workingblocks AS $WorkingBlockKey => $WorkingblockValue) {
                $WorkingBlockSet[] = array(
                    'WorkingBlockID' => $WorkingblockValue['WorkingBlockID'],
                    'StartTime' => $WorkingblockValue['StartTime'],
                    'EndTime' => $WorkingblockValue['EndTime'],
                    'EmployeeID' => $WorkingblockValue['EmployeeID'],
                );
            }
            $EmployeeWorkingblocks[] = array(
                'EmployeeID' => $EmployeeValue['EmployeeD'],
                'EmployeeName' => $EmployeeValue['EmployeeName'],
                'Picture' => $EmployeeValue['Picture'],
                'ColorCode' => $EmployeeValue['ColorCode'],
                'WorkingBlocks' => $WorkingBlockSet
            );
            unset($WorkingBlockSet);

        }
        $Availability = json_encode($EmployeeWorkingblocks);
        return $EmployeeWorkingblocks;
    }

    protected function reservations()
    {
        $Db = new Database;

        $CurrentReservationData = $Db->select("SELECT a.ReservationDate, a.ReservationID, a.TreatmentID, b.TreatmentName, b.ProductArray, CONCAT(c.FirstName, \" \", c.Prefix, \" \", c.LastName) AS CustomerName FROM `HL_Reservations` a LEFT JOIN `HL_Treatments` b ON a.TreatmentID = b.TreatmentID LEFT JOIN `HL_Customers` c ON a.CustomerID = c.CustomerID WHERE TreatmentName != '' AND a.ReservationDate BETWEEN NOW() + INTERVAL -7 DAY AND NOW() + INTERVAL 60 DAY  ORDER BY ReservationDate");
        foreach ($CurrentReservationData AS $ReservationDataKey => $ReservationDataValue) {
            //echo $ReservationDataKey ." -> ". $ReservationDataValue['ReservationDate'] ." ____> ". $ReservationDataValue['TreatmentName'] ." <br/>";


            $CurrentProductData = $Db->select("SELECT a.*, b.*, c.* FROM `HL_ReservationDetails` a 
                          LEFT JOIN `HL_Products` b ON a.ProductID = b.ProductID 
                          LEFT JOIN `HL_Employees` c ON a.EmployeeID = c.EmployeeID 
                          WHERE a.ReservationID = '" . $ReservationDataValue['ReservationID'] . "' 
                          ORDER BY NewStartTime");

            foreach ($CurrentProductData as $ProductKey => $ProductValue) {
                // echo $ProductValue['ProductName']." " . $ProductValue['NewStartTime'] . " - " . $ProductValue['NewEndTime'] . "   " . $ProductValue['FirstName'] . " " . $ProductValue['LastName'] . " <br/>";
                $ReservationProductSet[] = array(
                    'title' => $ReservationDataValue['CustomerName'] . "\n" . $ProductValue['FirstName'] . " " . $ProductValue['LastName'] . "\n" . $ReservationDataValue['TreatmentName'] . ": " . $ProductValue['ProductName'],
                    'start' => $ProductValue['ReservationDate'] . "T" . $ProductValue['NewStartTime'],
                    'end' => $ProductValue['ReservationDate'] . "T" . $ProductValue['NewEndTime'],
                    'Duration' => $ProductValue['Duration'],
                    'id' => $ProductValue['ProductID'],
                    'ProductCode' => $ProductValue['ProductCode'],
                    'Employee' => $ProductValue['FirstName'] . " " . $ProductValue['Prefix'] . " ".$ProductValue['LastName'],
                    'EmployeeID' => $ProductValue['EmployeeID'],
                    'color' => $ProductValue['colorCode'],
                    'ReservationDetailID' => $ProductValue['ReservationDetailID']


                );
            }

            $ReservationDataSet[] = array(
                'ReservationID' => $ReservationDataValue['ReservationDate'],
                'TreatmentName' => $ReservationDataValue['TreatmentName'],
                'CustomerData' => $ReservationDataValue['CustomerName'],
                'ReservationProductSet' => $ReservationProductSet);
            //echo " <br/>";

            //
        }
        //print_r($ReservationProductSet);
        $Reservations = json_encode($ReservationProductSet);
        //var_dump(json_encode($ReservationProductSet));
        //unset($ReservationProductSet);
        return $ReservationProductSet;

        //return $ReservationProductSet;


    }


    protected function reservationsTreatments()
    {

        $Db = new Database;
        $CurrentReservationData = $Db->select("SELECT a.ReservationDate, a.ReservationID, a.TreatmentID, b.TreatmentName, b.ProductArray, CONCAT(c.FirstName, \" \", c.Prefix, \" \", c.LastName) AS CustomerName FROM `HL_Reservations` a LEFT JOIN `HL_Treatments` b ON a.TreatmentID = b.TreatmentID LEFT JOIN `HL_Customers` c ON a.CustomerID = c.CustomerID WHERE ReservationDate BETWEEN NOW() AND NOW() + INTERVAL 60 DAY  ORDER BY ReservationDate");
        foreach ($CurrentReservationData AS $ReservationDataKey => $ReservationDataValue) {

            $CurrentProductData = $Db->select("SELECT a.*, b.*, c.* FROM `HL_ReservationDetails` a 
                          LEFT JOIN `HL_Products` b ON a.ProductID = b.ProductID 
                          LEFT JOIN `HL_Employees` c ON a.EmployeeID = c.EmployeeID 
                          WHERE a.ReservationID = '" . $ReservationDataValue['ReservationID'] . "' 
                          ORDER BY NewStartTime");

            foreach ($CurrentProductData as $ProductKey => $ProductValue) {
                //echo $ProductValue['ProductName']." " . $ProductValue['NewStartTime'] . " - " . $ProductValue['NewEndTime'] . "   " . $ProductValue['FirstName'] . " " . $ProductValue['LastName'] . " <br/>";
                $ReservationProductSet[] = array(
                    'ProductName' => $ProductValue['ProductName'],
                    'StartTime' => $ProductValue['NewStartTime'],
                    'EndTime' => $ProductValue['NewEndTime'],
                    'Duration' => $ProductValue['Duration'],
                    'ProductCode' => $ProductValue['ProductCode'],
                    'Employee' => $ProductValue['FirstName'] . " " . $ProductValue['LastName'],
                    'EmployeeID' => $ProductValue['EmployeeID']
                );
            }

            $ReservationDataSet[] = array(
                'ReservationID' => $ReservationDataValue['ReservationDate'],
                'TreatmentName' => $ReservationDataValue['TreatmentName'],
                'CustomerData' => $ReservationDataValue['CustomerName'],
                'ReservationProductSet' => $ReservationProductSet);
            //echo " <br/>";

            unset($ReservationProductSet);
        }

        //$Reservations = json_encode($ReservationDataSet);

        return $ReservationDataSet;


    }

}

// Requests from the same server don't have a HTTP_ORIGIN header
if (!array_key_exists('HTTP_ORIGIN', $_SERVER)) {
    $_SERVER['HTTP_ORIGIN'] = $_SERVER['SERVER_NAME'];
}

try {
    $API = new MyAPI($_REQUEST['request'], $_SERVER['HTTP_ORIGIN']);
    echo $API->processAPI();
} catch (Exception $e) {
    echo json_encode(Array('error' => $e->getMessage()));
}